// Copyright 2014 a2Go/zhgk.  All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package utilGo

import (
	"testing"
	"time"
)

func TestGetNowTimeUnix(t *testing.T) {
	println(GetNowTimeUnix())
}

func TestGetNowTimeUnixNano(t *testing.T) {
	println(GetNowTimeUnixNano())
}

func TestGetNowTimeToIntStr(t *testing.T) {
	println(GetNowTimeToIntStr())
}

func TestGetNowTimeToStr(t *testing.T) {
	println(GetNowTimeToStr())
}

func TestGetNowTimeForFormatStr(t *testing.T) {
	println(GetNowTimeForFormatStr(Year))
	println(GetNowTimeForFormatStr(YearDashMonth))
	println(GetNowTimeForFormatStr(YearDashMonthDashDay))
	println(GetNowTimeForFormatStr(YearDashMonthDashDaySpaceHour))
	println(GetNowTimeForFormatStr(YearDashMonthDashDaySpaceHourColonMin))
	println(GetNowTimeForFormatStr(YearDashMonthDashDaySpaceHourColonMinColonSec))
	println(GetNowTimeForFormatStr(YearMonth))
	println(GetNowTimeForFormatStr(YearMonthDay))
	println(GetNowTimeForFormatStr(YearMonthDayHour))
	println(GetNowTimeForFormatStr(YearMonthDayHourMin))
	println(GetNowTimeForFormatStr(YearMonthDayHourMinSec))
}

func TestGetPreDay(t *testing.T) {
	println(GetPreDay(1))
}

func TestGetAfterDay(t *testing.T) {
	println(GetAfterDay(2))

}

func TestGetAfterDayForFormat(t *testing.T) {
	println(GetAfterDayForFormat(1, YearDashMonthDashDay))
}

func TestTest(t *testing.T) {
	println(time.Now().Weekday())
}

func TestGetNowWeekFirstDay(t *testing.T) {
	println(GetNowWeekFirstDay())
}

func TestGetNowWeekLastDay(t *testing.T) {
	println(GetNowWeekLastDay())
}

func Test(t *testing.T) {
	//var a float64 = 0.345323
	//str := string(a)
	//println(str)
}
