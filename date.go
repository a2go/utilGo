// Copyright 2014 a2Go/zhgk.  All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package utilGo

import (
	"time"
)

// 日期工具类
// 提供国内习惯使用的日期格式
// 使用方式：
//	import git.oschina.net/a2go/utilGo
//	utilGo.GetNowTimeToStr()

//定义日期格式类型
type dateFormat string

//初始化日期格式
const (
	//返回2016
	Year dateFormat = "2006"
	//返回201610
	YearMonth dateFormat = "200601"
	//返回20161024
	YearMonthDay dateFormat = "20060102"
	//返回2016102417
	YearMonthDayHour dateFormat = "2006010215"
	//返回201610241721
	YearMonthDayHourMin dateFormat = "200601021504"
	//返回20161024172155
	YearMonthDayHourMinSec dateFormat = "20060102150405"
	//返回2016-10
	YearDashMonth dateFormat = "2006-01"
	//返回2016-10-24
	YearDashMonthDashDay dateFormat = "2006-01-02"
	//返回2016-10-24 15
	YearDashMonthDashDaySpaceHour dateFormat = "2006-01-02 15"
	//返回2016-10-24 15:24
	YearDashMonthDashDaySpaceHourColonMin dateFormat = "2006-01-02 15:04"
	//返回2016-10-24 15:24:33
	YearDashMonthDashDaySpaceHourColonMinColonSec dateFormat = "2006-01-02 15:04:05"
)

//GetNowTimeUnix 返回自1970-01-01之后的秒数
func GetNowTimeUnix() int64 {
	return time.Now().Unix()
}

//GetNowTimeUnixNano 返回自1970-01-01之后的纳秒数
func GetNowTimeUnixNano() int64 {
	return time.Now().UnixNano()
}

// GetNowTimeToStr 返回时间样例：2006-02-03 15:04:06
func GetNowTimeToStr() string {
	return GetNowTimeForFormatStr(YearDashMonthDashDaySpaceHourColonMinColonSec)
}

//GetNowTimeToIntStr 返回日期格式 20161015
func GetNowTimeToIntStr() string {
	return GetNowTimeForFormatStr(YearMonthDayHourMinSec)
}

//GetCurrentDay 返回日期:2016-10-15
func GetCurrentDay() string {
	return GetNowTimeForFormatStr(YearDashMonthDashDay)
}

//GetCurrentMonth 返回月份:2016-10
func GetCurrentMonth() string {
	return GetNowTimeForFormatStr(YearDashMonth)
}

//GetCurrentDayT 返回日期：20160102
func GetCurrentDayT() string {
	return GetNowTimeForFormatStr(YearMonthDay)

}

//GetCurrentMonthT 返回月份:201610
func GetCurrentMonthT() string {
	return GetNowTimeForFormatStr(YearMonth)
}

//GetNowTimeForFormatStr 根据指定格式返回日期
//日期格式为dateFormat类型，从Const中选择
func GetNowTimeForFormatStr(format dateFormat) string {
	return time.Now().Format(string(format))
}

//GetPreDay 返回当前日期的前几天日期 默认格式：2006-02-03 15:04:06
func GetPreDay(day time.Duration) string {
	return GetPreDayForFormat(day, YearDashMonthDashDay)
}

//GetAfterDay 返回当前日期的后几天 ,默认格式：2006-02-03 15:04:06
func GetAfterDay(day time.Duration) string {
	return GetAfterDayForFormat(day, YearDashMonthDashDay)
}

//GetPreDayForFormat 返回指定格式的前几天日期
func GetPreDayForFormat(day time.Duration, format dateFormat) string {
	return GetAfterDayForFormat(-day, format)
}

//GetAfterDayForFormat 返回指定格式的后几天日期
func GetAfterDayForFormat(day time.Duration, format dateFormat) string {
	return time.Now().Add(day * 24 * time.Hour).Format(string(format))
}

//GetNowWeekFirstDay 返回本周的第一天，从星期一开始
func GetNowWeekFirstDay() string {

	dayIndex := int(time.Now().Weekday())
	if dayIndex == 0 {
		return GetPreDay(6)
	}
	return GetPreDay(time.Duration(dayIndex - 1))

}

//GetNowWeekLastDay 返回本周的最后一天，星期天的日期
func GetNowWeekLastDay() string {
	dayIndex := int(time.Now().Weekday())
	if dayIndex == 0 {
		return GetAfterDay(0)
	}
	return GetAfterDay(time.Duration(7 - dayIndex))

}
