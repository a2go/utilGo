// Copyright 2014 a2Go/zhgk.  All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.
package utilGo

import "testing"

func TestMd5(t *testing.T) {
	println(Md5("123456"))
	println(Md5("123456"))
	println(Md5("123456"))
}
func TestMd5Lower(t *testing.T) {
	println(Md5Lower("123456"))
	println(Md5Lower("123456"))
	println(Md5Lower("123456"))
}
func TestMd5Upper(t *testing.T) {
	println(Md5Upper("123456"))
	println(Md5Upper("123456"))
	println(Md5Upper("123456"))
}

func TestSHA1(t *testing.T) {
	println(SHA1("123456"))
}

func TestSHA256(t *testing.T) {
	println(SHA256("123456"))
}

func TestSHA512(t *testing.T) {
	println(SHA512("123456"))
	var i interface{} = 354
	var g = int64(i.(int))
	println(g)
}
