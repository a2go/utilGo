// Copyright 2014 a2Go/zhgk.  All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package utilGo

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"strings"
)

//Md5 对指定的字符串进行MD5加密
func Md5(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	md5Byte := h.Sum(nil)
	md5Str := hex.EncodeToString(md5Byte)
	return md5Str
}

//Md5Upper 返回大写的MD5十六进制加密后字符串
func Md5Upper(str string) string {
	return strings.ToUpper(Md5(str))
}

//Md5Lower 返回小写的MD5十六进制加密后字符串
func Md5Lower(str string) string {
	return strings.ToLower(Md5(str))
}

//SHA1 对给定的字符串进行SHA1加密
func SHA1(str string) string {
	s := sha1.New()
	s.Write([]byte(str))
	md5Byte := s.Sum(nil)
	sha1Str := hex.EncodeToString(md5Byte)
	return sha1Str
}

//SHA512 对给定的字符串进行SHA512加密
func SHA512(str string) string {
	s := sha512.New()
	s.Write([]byte(str))
	md5Byte := s.Sum(nil)
	sha1Str := hex.EncodeToString(md5Byte)
	return sha1Str
}

//SHA256 对给定的字符串进行SHA256加密
func SHA256(str string) string {
	s := sha256.New()
	s.Write([]byte(str))
	md5Byte := s.Sum(nil)
	sha1Str := hex.EncodeToString(md5Byte)
	return sha1Str
}
