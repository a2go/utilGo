// Copyright 2014 a2Go/zhgk.  All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.
package utilGo

import "testing"

func TestString(t *testing.T) {
	println(String(124.0900001))
}
func TestFormatFloat(t *testing.T) {
	var f float32 = 23.3115345
	println(FormatFloat(f, 3))

	var f2 = 23.3115345
	println(FormatFloat(f2, 3))
}
